FROM jekyll/jekyll:3.8.5
LABEL maintainer "Nobu Aoki <aoki@sparqlite.com>"

RUN apk update && \
  apk add --no-cache  libtool automake autoconf nasm

RUN npm install --global webpack webpack-cli typescript
